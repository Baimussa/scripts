#!/bin/sh
echo "Proc: $(nproc)"
echo "Mem: $(free -h | grep Mem | awk '{print $2}')"
sudo fdisk -l | grep Disk | grep /dev/sd | awk '{print $1" "$2" "$3$4}'
ethtool ens192 | grep -i speed
